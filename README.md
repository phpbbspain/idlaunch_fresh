IDLaunch Fresh 
=========================

IDLaunch Fresh style phpBB 3.1

Requirements:
- phpBB 3.1
- prosilver

IDLaunch Fresh
==========================

Estilo IDLaunch Fresh phpBB 3.1

Requiere:
- phpBB 3.1
- prosilver

## Corrección de errores
Para cualquier cambio a realizar, simplemente editar para realizar el cambio y "Pull Request".

## Estilo validado por phpBB Oficial
[Tema Oficial del estilo (Ingles)](https://www.phpbb.com/community/viewtopic.php?f=531&t=2314506) - 
[Soporte Oficial del estilo (Ingles)](https://www.phpbb.com/customise/db/style/idlaunch_fresh_2/support)

## Algunas capturas
![phpBB Spain](http://www.phpbb-es.com/archivos/IDLaunch_Fresh_new.png)
![phpBB Spain](http://www.phpbb-es.com/archivos/IDLaunch_Fresh_new2.png)
![phpBB Spain](http://www.phpbb-es.com/archivos/IDLaunch_Fresh_new3.png)

## [Demo Online - IDLaunch Fresh](http://area51.phpbb-es.com/foro/index.php?style=26)

## License
[GNU General Public License v2](http://opensource.org/licenses/GPL-2.0)

## Autor
[FreakyBlue](https://www.phpbb.com/community/memberlist.php?mode=viewprofile&u=680545)

Ported phpBB 3.1.x - ThE KuKa (Raúl Arroyo Monzo) - [phpBB Spain](http://www.phpbb-es.com)

![phpBB Spain](http://www.phpbb-es.com/images/logo_es.png) 
